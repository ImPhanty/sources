const fs = require('fs').promises;
const path = require('path');
const crypto = require('crypto');

const iconsPath = path.resolve(__dirname, '..', 'public', 'icons');

(async () => {
  const icons = await Promise.all(
    (await fs.readdir(iconsPath))
      .map(async name => {
        const buffer = await fs.readFile(path.resolve(iconsPath, name), { encoding: null });
        const sha1 = crypto.createHash('sha1').update(buffer).digest('hex');
        return { name, sha1 };
      })
  );
  await fs.writeFile(path.resolve(__dirname, '..', 'public', 'icons.json'), JSON.stringify(icons));
})();
